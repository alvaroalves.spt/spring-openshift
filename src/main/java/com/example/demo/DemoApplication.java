package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import java.net.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		try{
		return InetAddress.getLocalHost().toString() + "<br>TESTE OPENSHIFT.";
		}
		catch(Exception e){

		}
		return "Bem vindo ao futuro";
		
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
